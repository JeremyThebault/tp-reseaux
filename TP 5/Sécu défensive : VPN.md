### TP5
## Sécu défensive : VPN


### 1. Ping

Tout d'abord avant de faire quoique se soit, il faut parametrer l'IP de la rasberry pi en lui mettant une addresse IP static.


En faisant la commande ```ifconfig``` on peut voir les adresses IP.

Pour remplacer l'adresse Ip que le routeur nous a donner on va taper la commande : 
```sudo nano/etc/dhcpd.conf```

Tout en bas de se fichier nous allons rajouter :
```
interface eth0
static ip_address=192.168.2.45/24
static routers=192.168.2.1
static domain_name_servers=192.168.2.1

```
Une fois cela fait, pour bien enregistrer les modifications du fichier nous allons redemarrer la Rasberry pi avec la commande :```sudo reboot```

Une fois que l'IP de la rasberry pi est static, on la branche a notre PC par un cable ethernet.

Pour voir si ce la fonctionne on a juste a faire dans un powershell ou dans un terminal ```ping``` avec l'adresse IP que l'on souhaite ping :

```
Statistiques Ping pour 1.1.1.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 10ms, Maximum = 16ms, Moyenne = 12ms
PS C:\Users\alexi> ping google.com

Envoi d’une requête 'ping' sur google.com [142.250.179.110] avec 32 octets de données :
Réponse de 142.250.179.110 : octets=32 temps=11 ms TTL=120
Réponse de 142.250.179.110 : octets=32 temps=9 ms TTL=120
Réponse de 142.250.179.110 : octets=32 temps=13 ms TTL=120
Réponse de 142.250.179.110 : octets=32 temps=12 ms TTL=120

Statistiques Ping pour 142.250.179.110:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 9ms, Maximum = 13ms, Moyenne = 11ms
```

 
### 2. Installation de WireGuard et génération d'une paire de clés.

Une fis que la rasberry pi et le PC communique entre eux nous pouvons commencer a installer WireGuard dans la Rasberry pi.

- **A/ WireGuard**

Premièrement avant de l'installer, on ajoute deux référentiels logiciels supplémentaires à l'index des paquets de votre serveur epel, et elrepo. On Exécutez la commande suivante pour les installer.

```
sudo dnf install elrepo-release epel-release
```

Maintenant que le serveur a accès au référentiel qui héberge le package WireGuard, on peut installez WireGuard à l'aide de la commande si dessous : 
```
sudo dnf install kmod-wireguard wireguard-tools
```

- **B/ génération des paire de clés**

Maintenant que WireGuard est installé, l'étape suivante consiste à générer une paire de clés privée et publique pour le serveur.

**Clé privée** :
Commande : ```wg genkey | sudo tee /etc/wireguard/private.key```
La clé :```AFlJhnJg1m3OZ5wxo6nXAfNvg+IvnGiPrDOh1k/XHEc=```

- Avec la commande ```sudo chmod go= /etc/wireguard/private.key``` on change les autorisations de la clé privée.


Ce qui supprime toutes les autorisations sur le fichier pour les utilisateurs et les groupes autres que root afin de s'assurer que lui seul puisse accéder à la clé privée.

**Clé public** :
Commande : ```sudo cat /etc/wireguard/private.key | wg pubkey | sudo tee /etc/wireguard/public.key```
La clé : ```oK8d9wBzNiUFjtdyKrzsatv364BHpX3Djj4PMoW3qTY=```

### 2. Paramétrage de WireGuard serveur (ip range).

configuration de l'ipv4 : 
* ipv4 = 192.168.2.45/24

### 3. Création d'une configuration de serveur WireGuard


Une fois qu'on a la clé privée et l'adresse IP requise, on va créer un nouveau fichier de configuration à l'aide de nano en exécutant la commande suivante :

```sudo nano /etc/wireguard/wg0.conf```

On mettra dans se fichier notre clé privé dans ```PrivateKey```et l'addresse ip dans ```Address```

```
sudo cat /etc/wireguard/wg0.conf
[Interface]
PrivateKey = YMA18YMFE6z6miXaWWv/ZDMswd7HPWmKNwNoZ/q+Qm4=
Address = 10.190.75.1/24
MTU = 1420
ListenPort = 51820
SaveConfig = false
DNS = 1.1.1.1
```


###  4. Configuration d'un homologue pour acheminer tout le trafic sur le tunnel

Pour les pairs distants accessibles via SSH ou un autre protocole utilisant des adresses IP publiques, des règles supplémentaires doivent être ajoutées au fichier wg0.conf du pair. Ces règles garantissent qu'on peut toujours se connecter au système depuis l'extérieur du tunnel, si on est connecté.

WireGuard a la capacité d'exécuter automatiquement les commandes Windows spécifiées dans les options PreUp, PostUp, PreDownet PostDowndans la configuration du tunnel.

```
[Interface]
PrivateKey = AFlJhnJg1m3OZ5wxo6nXAfNvg+IvnGiPrDOh1k/XHEc=
Address = 10.190.75.8/24
DNS = 1.1.1.1
PostUp = powershell -command "$wgInterface = Get-NetAdapter -Name %WIREGUARD_TUNNEL_NAME%; route add 0.0.0.0 mask 0.0.0.0 10.190.75.1 IF $wgInterface.ifIndex metric"
PreDown = powershell -command "$wgInterface = Get-NetAdapter -Name %WIREGUARD_TUNNEL_NAME%; route delete 0.0.0.0 mask 0.0.0.0 10.190.75.1 if $wgInterface.ifIndex metric"

[Peer]
PublicKey = 3fEP7nVTJ7rU+hfyTyGfnAaLUuu9cozFjRPSECx2838=
AllowedIPs = 10.190.75.0/24
Endpoint = 192.168.2.45:51820

```
on a fait un ip a sur la rasberry 

```alexis@raspberrypi:~ $ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether dc:a6:32:40:ba:6b brd ff:ff:ff:ff:ff:ff
    inet 192.168.2.45/24 brd 192.168.2.255 scope global noprefixroute eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::8f4f:a528:ee30:97c1/64 scope link
       valid_lft forever preferred_lft forever
3: wlan0: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether dc:a6:32:40:ba:6d brd ff:ff:ff:ff:ff:ff
4: wg0: <POINTOPOINT,NOARP,UP,LOWER_UP> mtu 1420 qdisc noqueue state UNKNOWN group default qlen 1000
    link/none
    inet 10.190.75.1/24 scope global wg0
       valid_lft forever preferred_lft forever
```
       
       
voici notre table de routage:
```
IPv4 Table de routage
===========================================================================
Itinéraires actifs :
Destination réseau    Masque réseau  Adr. passerelle   Adr. interface Métrique
          0.0.0.0          0.0.0.0      10.190.75.1      10.190.75.8      6
         10.3.1.0    255.255.255.0         On-link          10.3.1.1    281
         10.3.1.1  255.255.255.255         On-link          10.3.1.1    281
       10.3.1.255  255.255.255.255         On-link          10.3.1.1    281
       ...
```
ping client vers raspberry:

```
Envoi d’une requête 'Ping'  192.168.2.45 avec 32 octets de données :
Réponse de 192.168.2.45 : octets=32 temps=1 ms TTL=64
Réponse de 192.168.2.45 : octets=32 temps=1 ms TTL=64
Réponse de 192.168.2.45 : octets=32 temps<1ms TTL=64
Réponse de 192.168.2.45 : octets=32 temps=1 ms TTL=64

Statistiques Ping pour 192.168.2.45:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 1ms, Moyenne = 0ms
```

ping raspberry vers client:

```
Envoi d’une requête 'Ping'  192.168.2.50 avec 32 octets de données :
Réponse de 192.168.2.50 : octets=32 temps=1 ms TTL=64
Réponse de 192.168.2.50 : octets=32 temps=1 ms TTL=64
Réponse de 192.168.2.50 : octets=32 temps<1ms TTL=64
Réponse de 192.168.2.50 : octets=32 temps=1 ms TTL=64

Statistiques Ping pour 192.168.2.50:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 1ms, Moyenne = 0ms
```


![](https://i.imgur.com/97uzN8M.jpg)
