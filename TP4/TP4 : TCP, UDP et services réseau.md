# TP4 : TCP, UDP et services réseau
## I/ First steps
**Les 5 applications utilisées :**

```
- Hyperplanning
- Minecraft
- Microsoft Store
- Steam
- Discord
```

* **Hyperplanning** 

Une page web comme Hyperplanning c’est du TCP.
Ip auquel on peut se connecter : 96.16.122.67 sur le port 80.
Plusieurs ports ont été ouvert ici tels que 50820/1/2/3.

Avis de l'OS :
```PS C:\WINDOWS\system32> netstat -a -b -n

Active Connections

  Proto  Local Address          Foreign Address        State
  ...
  TCP    192.168.137.145:50820  96.16.122.67:80        TIME_WAIT
  TCP    192.168.137.145:50821  96.16.122.67:80        TIME_WAIT
  TCP    192.168.137.145:50822  96.16.122.67:80        TIME_WAIT
  TCP    192.168.137.145:50823  96.16.122.67:80        TIME_WAIT
  TCP    192.168.137.145:50826  178.32.154.10:443      ESTABLISHED
 [brave.exe]
...
```

🦈[PCAP Hyperplanning](https://gitlab.com/JeremyThebault/tp-reseaux/-/blob/main/TP4/Hyperplanning_tp4.pcapng)

* **Minecraft**

Minecraft en Multi c’est du TCP.
Ip auquel on peut se connecter : 185.116.156.246 sur le port 37629.
Le port local qui a été ouvert ici est le 51902.


Avis de l'OS :
```
PS C:\WINDOWS\system32> netstat -a -b -n

Active Connections

  Proto  Local Address          Foreign Address        State
  ...
 TCP    192.168.137.145:51902  185.116.156.246:37629  ESTABLISHED
 [javaw.exe]
...
```
🦈[PCAP Minecraft](https://gitlab.com/JeremyThebault/tp-reseaux/-/blob/main/TP4/mc_tp4.pcapng)

* **Microsoft Store** 
Ip auquel on peut se connecter : 20.99.128.106 sur le port 443.
Le port local qui a été ouvert ici est le 1404.

Avis de l'OS :
```
PS C:\WINDOWS\system32> netstat -a -b -n

Active Connections

  Proto  Local Address          Foreign Address        State
  ...
 TCP    10.33.16.31:1404       20.99.128.106:443      TIME_WAIT
 TCP    10.33.16.31:1405       23.57.5.23:443         ESTABLISHED
 [WinStore.App.exe]
...
```
🦈[PCAP Microsoft Store](https://gitlab.com/JeremyThebault/tp-reseaux/-/blob/main/TP4/microsoftstore_tp4.pcapng)


* **Steam** 
Quand ça télécharge un jeu c'est de l'UDP (entre autres, parce qu'il y a beaucoup de connection qui s'effectue comme du web ...).
Ip auquel on peut se connecter : 185.25.182.76 sur le port 27017


Avis de l'OS :
```
PS C:\WINDOWS\system32> netstat -a -b -n

Active Connections

  Proto  Local Address          Foreign Address        State
  ...
 UDP    0.0.0.0:55551          :
 [steam.exe]
...
```
🦈[PCAP Steam](https://gitlab.com/JeremyThebault/tp-reseaux/-/blob/main/TP4/steamdl_tp4.pcapng)

* **Discord**

(quand on envoie des messages), c'est de l'UDP (en vrai c'est plutôt du QUIC)*  

IP vers laquelle nous pouvons nous connecter: 162.159.128.233 sur le port 443
Le port local qui a été ouvert ici est le 65032

Avis de l'OS :
```
PS C:\WINDOWS\system32> netstat -a -b -p UDP

Active Connections

  Proto  Local Address          Foreign Address        State
  ...
  UDP    0.0.0.0:65032          *:*
 [Discord.exe]
```

🦈[PCAP Discord](https://gitlab.com/JeremyThebault/tp-reseaux/-/blob/main/TP4/discordmsg_tp4.pcapng)


## II/ Mise en place

### 1/ SSH

Il est évident que SSH utilise TCP car nous voulons que les commandes envoyées soient intégrables et qu'elles n'arrivent pas dans le désordre par exemple.

🦈[PCAP SSH](https://gitlab.com/JeremyThebault/tp-reseaux/-/blob/main/TP4/ssh_tp4.pcapng)

Demandez aux OS :

Sur Windows:
```
PS C:\WINDOWS\system32> netstat -a -b -n -p TCP

Active Connections

  Proto  Local Address          Foreign Address        State
 TCP    10.3.1.1:11732         10.3.1.11:22           ESTABLISHED
 [ssh.exe]
```

Sur la VM:
```
[jeremy@localhost ~]$ ss -t
State                          Recv-Q                           Send-Q                                                     Local Address:Port                                                     Peer Address:Port                           Process
ESTAB                          0                                52                                                             10.3.1.11:ssh                                                          10.3.1.1:11732
```
Le port 22 est traduit par ssh

### 2/ NFS

Sur une machine virtuelle en tant que serveur NFS :

```
[jeremy@localhost ~]$ sudo dnf -y install nfs-utils
...
Complete!
[jeremy@localhost ~]$ sudo vim /etc/exports
[jeremy@localhost /]$ sudo mkdir /srv/shared
[jeremy@localhost ~]$ sudo systemctl enable --now rpcbind nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
[jeremy@localhost /]$ sudo firewall-cmd --add-service=nfs
success
```

Sur la VM client:
```
[jeremy@localhost ~]$ sudo dnf -y install nfs-utils
...
Complete!
[jeremy@localhost ~]$ sudo mount -t nfs 10.3.1.254:/home/jeremy/shared /mnt
[jeremy@localhost ~]$ df -hT
...
10.3.1.254:/home/jeremy/shared nfs4      6.2G  1.2G  5.1G  18% /mnt
```

🦈[PCAP NFS](https://gitlab.com/JeremyThebault/tp-reseaux/-/blob/main/TP4/nfs_tp4.pcapng)

Le port utilisé par le serveur est donc le 2049.

Demandez aux OS

Sur le serveur:
```
State                          Recv-Q                           Send-Q                                                     Local Address:Port                                                     Peer Address:Port                           Process
ESTAB                          0                                0                                                             10.3.1.254:2049                                                        10.3.1.11:702
```
Sur le client:
```
[jeremy@localhost ~]$ ss -t
State                          Recv-Q                          Send-Q                                                   Local Address:Port                                                        Peer Address:Port                           Process
ESTAB                          0                               0                                                            10.3.1.11:ssh                                                           10.3.1.254:40070
ESTAB                          0                               0                                                            10.3.1.11:iris-beep                                                     10.3.1.254:nfs
```
Le port nfs a été traduit du numéro 2049 et iris-beep par 702

### 3. DNS
```
[jeremy@localhost ~]$ sudo tcpdump -i enp0s3 -w tp4_dns.pcap not port 22
[jeremy@localhost ~]$ dig ynov.com
...
```

Notez que l'adresse IP du serveur DNS auquel vous vous connectez est 8.8.8.8 sur le port 53.
